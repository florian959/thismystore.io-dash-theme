var gulp = require("gulp");

gulp.task("default", function(done) {
  // include bootstrap JS & CSS
  gulp
    .src("./node_modules/bootstrap/dist/css/bootstrap.min.css")
    .pipe(gulp.dest("./css"));
  gulp
    .src("./node_modules/bootstrap/dist/js/bootstrap.min.js")
    .pipe(gulp.dest("./js"));

  // include jquery JS
  gulp.src("./node_modules/jquery/dist/jquery.min.js").pipe(gulp.dest("./js"));

  // include popper.js JS
  gulp
    .src("./node_modules/popper.js/dist/popper.min.js")
    .pipe(gulp.dest("./js"));

  // include font awesome CSS & fonts
  gulp.src("./node_modules/font-awesome/fonts/*").pipe(gulp.dest("./fonts/"));
  gulp
    .src("./node_modules/font-awesome/css/font-awesome.min.css")
    .pipe(gulp.dest("./css/"));

  // it's ok
  console.log("--- All fonts, CSS & JS files copied ---");
  done();
});
